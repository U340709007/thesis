import glob
import re

import numpy as np

import cv2
from matplotlib import pyplot as plt


def calculate_histogram_threshold(image, percentage=0.005):
    sum = 0
    hist = cv2.calcHist([image], [0], None, [256], [0, 256])
    for i in range(0, 256):
        hist[i]
        #print(hist[i][0],i)
        sum += hist[i][0]
        perc = sum/(image.size)
        if perc > percentage:
            num = i
            break
    return num


def pixel_color_check(g):
    image = cv2.imread(g)
    image_blur = cv2.GaussianBlur(image, (11,11),0)

    grayscaled_noblur_nothreshold1 = cv2.cvtColor(image_blur, cv2.COLOR_RGB2GRAY)

    kernel = np.ones((1, 5), np.uint8)
    grayscaled_noblur_nothreshold_morphed2 = cv2.morphologyEx(grayscaled_noblur_nothreshold1, cv2.MORPH_ERODE, kernel, iterations=1)
    kernel = np.ones((3, 1), np.uint8)
    grayscaled_noblur_nothreshold_morphed = cv2.morphologyEx(grayscaled_noblur_nothreshold_morphed2, cv2.MORPH_ERODE, kernel, iterations=1)

    num = calculate_histogram_threshold(grayscaled_noblur_nothreshold_morphed, 0.005)
    retval, grayscaled = cv2.threshold(grayscaled_noblur_nothreshold_morphed, num, 255, cv2.THRESH_BINARY_INV)

    #grayscaled = cv2.adaptiveThreshold(grayscaled_noblur_nothreshold, 255, cv2.ADAPTIVE_THRESH_MEAN_C,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, 7, 2)

    #cv2.imshow("Image1", image)
    #cv2.imshow("Image2", grayscaled_noblur_nothreshold_morphed1)
    #cv2.imshow("Image3", grayscaled_noblur_nothreshold_morphed)
    #cv2.imshow("Image4", grayscaled)
    #cv2.waitKey()

    hatali = False

    im2, contours, hierarchy = cv2.findContours(grayscaled, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

    for cnt in contours:
        area = cv2.contourArea(cnt)
        if area == 0:
            continue

        if area < 90:
            continue

        (x, y), radius = cv2.minEnclosingCircle(cnt)
        area2 = radius * radius * 3.14
        center = (int(x), int(y))
        radius = int(radius)

        if area2 == 0:
            continue

        (p, q, w, h) = cv2.boundingRect(cnt)
        area3 = w*h

        areaOranlari = area / area2
        areaOranlari2 = area / area3

        if areaOranlari > 0.33:
            hatali = True
            print("Hatali")
            if re.search("hatasiz",g):

                #FALSE POSITIVE INSPECTION

                #print("ContourArea/MinEnclosingArea :", areaOranlari)
                #print("CircleArea/RectArea :", areaOranlari2)
                #print("Contour Area :", cv2.contourArea(cnt))
                #cv2.imshow("Image_before :", image)
                #cv2.imshow("Grayscaled :", grayscaled)
                #cv2.circle(image, center, radius, (0, 255, 0), 2)
                #cv2.rectangle(image, (p, q), (p + w, q + h), (255, 0, 0), 2)
                #cv2.drawContours(image, cnt, -1, (255, 0, 0), 3)
                #cv2.imshow("Image_after :", image)
                #print(g)
                #cv2.waitKey()
                break
        elif re.search("hatali",g):
            print("Hatasiz")
            print("ContourArea/MinEnclosingArea", areaOranlari)
            print("Contour Area :", cv2.contourArea(cnt))
            # FALSE NEGATIVE INSPECTION
            #cv2.imshow("Grayscaled", grayscaled)
            #cv2.circle(image, center, radius, (0, 255, 0), 2)
            #cv2.rectangle(image, (p,q), (p+w,q+h), (255, 0, 0), 2)
            #cv2.drawContours(image, cnt, -1, (255, 0, 0), 3)
            #cv2.imshow("Image", image)
            #print(g)
            #cv2.waitKey()

    if hatali:
        with open("Output2.txt", 'a') as out:
            out.write(" BLACK FIND AT " + g + '\n')
            return True
    else:
            return False

file_names = glob.glob("images/hat*/*.png")
#file_names = glob.glob("element/1_element/1_face/124.png")

true_positive = 0
positive = 0
true_negative = 0
negative = 0

for i in range(0,len(file_names)):
    # image = cv2.imread(file_names[i])
    # #print(file_names[i])
    # num = show_grayscale_histogram(image)
    # print(num)
    # blur = cv2.bilateralFilter(image,9,75,75)
    #
    # grayscaled = cv2.cvtColor(blur,cv2.COLOR_BGR2GRAY)
    # retval, threshold = cv2.threshold(grayscaled, num, 255, cv2.FAST_FEATURE_DETECTOR_THRESHOLD)
    # blur2 = cv2.blur(threshold,(3,3))
    # cv2.imshow('threshold',blur2)
    # cv2.waitKey()

    isHatali=pixel_color_check(file_names[i]);
    if re.search("hatali",file_names[i]):
        positive = positive + 1
        if isHatali:
            true_positive = true_positive + 1
    elif re.search("hatasiz",file_names[i]):
        negative = negative + 1
        if not isHatali:
            true_negative = true_negative + 1

with open("Output2.txt", 'a') as out:
    out.write("Positive/Negative: "+str(positive)+"/"+str(negative)+"\n")
    out.write("TNR:"+str(true_negative / negative)+" TPR:"+str(true_positive / positive)+"\n")
    print("Positive/Negative: "+str(positive)+"/"+str(negative)+"\n")
    print("TNR:"+str(true_negative / negative)+" TPR:"+str(true_positive / positive)+"\n")