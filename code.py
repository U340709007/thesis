import cv2
import os
import numpy as np
from PIL import Image
import glob
from shutil import copyfile
import sys


FRAMECROP_1 = [165,380]
FRAMECROP_2 = [130,522]



# --------------------------------- # --------------------------------- # ---------------------------------
# step 1: videodan frame'lerin elde edilmesi ve RGB formatına (siyah-beyaz) çevrilmesi
# --------------------------------- # ---------------------------------  # ---------------------------------

def frame_to_rgb(video, path_output_dir_frame ,path_output_dir_mask):
    # extract frames from a video and save to directory as 'x.png' where
    # x is the frame index
    vidcap = cv2.VideoCapture(video)

    count1 = 0
    success = 1
    print(vidcap.read())
    while (success):
        success, image = vidcap.read()
        if not success:
            break

        image2 = image[FRAMECROP_1[0]:FRAMECROP_1[1], FRAMECROP_2[0]:FRAMECROP_2[1]]

        lower_red = np.array([160, 0, 0], np.uint8)
        upper_red = np.array([255, 255, 255], np.uint8)

        mask = cv2.inRange(image2, lower_red, upper_red)

        cv2.imwrite(os.path.join(path_output_dir_mask, '%d.png') % count1, mask)
        cv2.imwrite(os.path.join(path_output_dir_frame, '%d.png') % count1, image)
        count1 += 1
    vidcap.release()

# --------------------------------- # --------------------------------- # ---------------------------------
# step 2:
# --------------------------------- # ---------------------------------  # ---------------------------------

if not os.path.exists("frames"):
    os.makedirs("frames")
if not os.path.exists("masked_frames"):
    os.makedirs("masked_frames")
if not os.path.exists("control_frames"):
    os.makedirs("control_frames")
if not os.path.exists("control_frames_final"):
    os.makedirs("control_frames_final")


frame_to_rgb('{}'.format(sys.argv[1]), "frames", 'masked_frames')


# --------------------------------- # --------------------------------- # ---------------------------------
# step 2: sadece elementin olduğu görüntülerin belirlenmesi / RGB görüntülere sağdan ve soldan bakma
# --------------------------------- # ---------------------------------  # ---------------------------------


right = []
left = []

# RGB görüntülere soldan bakan fonksiyon.
def left_croping(file):
    image = Image.open(file).convert('RGB')
    left_cropped_image = image.crop((0,105,2,110))

    pix = left_cropped_image.load()
    w = left_cropped_image.size[0]
    h = left_cropped_image.size[1]
    bl = (0, 0, 0)
    wh = (255,255,255)

    pix_list=[]

    for i in range(w):
        for j in range(h):
            x = pix[i, j]
            pix_list.append(x)

    if wh not in pix_list:
        left.append(file.split("/")[-1])

# RGB görüntülere sağdan bakan fonksiyon.
def right_croping(file):
    image = Image.open(file).convert('RGB')
    right_cropped_image = image.crop((385,105,389,110))

    pix = right_cropped_image.load()
    w = right_cropped_image.size[0]
    h = right_cropped_image.size[1]
    bl = (0, 0, 0)
    wh = (255, 255, 255)

    pix_list=[]

    for i in range(w):
        for j in range(h):
            x = pix[i, j]
            pix_list.append(x)

    if wh not in pix_list:
        right.append(file.split("/")[-1])


def sortKeyFunc(s):
    return int(os.path.basename(s)[:-4])

FILENAMES = glob.glob('masked_frames/*.png')  # image can be in gif jpeg or png format
files=[]


for FILENAME in FILENAMES:
    files.append(sortKeyFunc(FILENAME))

for file in sorted(files):
    left_croping("masked_frames/{}.png".format(file))

for file in sorted(files):
    right_croping("masked_frames/{}.png".format(file))

common_list= list(set(right) & set(left))

for c in common_list:
    # image = Image.open("frames/{}".format(c))
    # image.save("control_frames/{}".format(c))
    copyfile("frames/{}".format(c), "control_frames/{}".format(c))


# --------------------------------- # --------------------------------- # ---------------------------------
# step 3: edge detection ile son görüntünün kırpılması
# --------------------------------- # ---------------------------------  # ---------------------------------

def xyz(control_FILENAME):
    img = cv2.imread("control_frames/{}".format(control_FILENAME.split("\\")[-1]))

    kernel = np.zeros((9, 9), np.float32)
    kernel[4, 4] = 2.0

    boxFilter = np.ones((9, 9), np.float32) / 81.0

    kernel = kernel - boxFilter
    custom = cv2.filter2D(img, -1, kernel)
    img = custom
    img2 = img[175:365, 125:522]

    blurred = cv2.blur(img2,(1,1))
    #blurred = cv2.bilateralFilter(img2,9,75,75)
    #blurred = cv2.GaussianBlur(img2,(5,5),0,0)
    canny = cv2.Canny(blurred, 50, 200)

    ## find the non-zero min-max coords of canny
    pts = np.argwhere(canny>0)
    y1,x1 = pts.min(axis=0)
    y2,x2 = pts.max(axis=0)

    ## crop the region
    cropped = img2[y1:y2, x1:x2]
    cv2.imwrite("control_frames_final/{}.png".format(control_FILENAME.split("\\")[-1].split(".")[0]), cropped)

control_FILENAMES = glob.glob('control_frames/*.png')
for control_FILENAME in control_FILENAMES:
    xyz(control_FILENAME)


# --------------------------------- # --------------------------------- # ---------------------------------
# step 4: element ve face Klasörlerine ayırma
# --------------------------------- # ---------------------------------  # ---------------------------------

def partition_to_elements():
    file_names = glob.glob("control_frames_final/*.png")

    for i, file in enumerate(file_names):
        file_names[i] = int(file.split("\\")[-1].split(".")[0])
    file_names = sorted(file_names)

    indexs = [0]

    for i, file in enumerate(file_names):
        if file_names[-1] == file_names[i]:
            break
        tmp = file_names[i + 1]
        diff = tmp - file
        if diff > 10:
            index = file_names.index(tmp)
            indexs.append(index)
    print(indexs)

    count = 0
    element = 1
    for i, file in enumerate(file_names):
        if i in indexs:
            count += 1
            if not os.path.exists("elements/{}_element/{}_face".format(element, count)):
                os.makedirs("elements/{}_element/{}_face".format(element, count))
            if count == 5:
                os.rmdir("elements/{}_element/{}_face".format(element, count))
                element += 1
                count = 1
                if not os.path.exists("elements/{}_element/{}_face".format(element, count)):
                    os.makedirs("elements/{}_element/{}_face".format(element, count))

        # image = Image.open("control_frames/{}.png".format(file))
        # image.save("{}_face/{}.png".format(count,file))
        copyfile("control_frames_final/{}.png".format(file), "elements/{}_element/{}_face/{}.png".format(element, count, file))

        # copyfile("control_frames/{}.png".format(file), "{}_face/{}.png".format(count,file))

partition_to_elements()